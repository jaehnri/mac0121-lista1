#include <stdio.h>

typedef struct {
  int * v;
  int topo;
  int tam;
} pilha;

pilha * criaPilha (int tam);
void destroiPilha (pilha * p);
void empilha (pilha * p, int x);
int desempilha (pilha * p);
int pilhaVazia (pilha * p);
int topoDaPilha (pilha *p);
pilha * aumentaPilha (pilha * p); 



