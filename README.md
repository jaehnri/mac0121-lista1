# Exercício 10 - lista 1 - MAC121

A entrada desse programa deve receber um inteiro n representando a quantidade de regiões do mapa, e uma matriz nxn separada por espacos, em que cada elemento, podemos ter 0 ou 1, indicando se há fronteira entre a região da linha e a região da coluna.

Exemplo de entrada:

6  
1 1 1 0 0 0  
1 1 1 1 1 1   
1 1 1 1 0 0   
0 1 1 1 1 1  
0 1 0 1 1 1  
0 1 0 1 1 1  
