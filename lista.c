#include <stdio.h>
#include <stdlib.h> 
#include "pilha.h"

int * alocaVetor (int tam) {
    return malloc(tam * sizeof(int));
}

int ** alocaMatriz (int lin, int col) {
  int ** mat = malloc (lin * sizeof(int *));
  int i;
  for (i = 0; i < lin; i++)
    mat[i] = malloc (col * sizeof(int));
  return (mat);
}

void liberaMatriz (int ** mat, int lin){
  int i;
  for (i = 0; i < lin; i++) free(mat[i]);
  free (mat);
}

void imprimeMatriz (int **a, int m, int n) {
    int i,j;
    for (i = 0; i < m; i++){
        for (j = 0; j < n; j++) printf("+---");
        printf("+\n");
        for (j = 0; j <n; j++)
          if(a[i][j] != 0) printf("|%3d", a[i][j]);
            else printf("|   ");
        printf("|\n");
    }
    for (j = 0; j < n; j++) printf("+---");
    printf("+\n");
}

int corDisponivel(int** mapa, int* cores, int regiao, int cor) {
    int i;
    /* Passar por todos os países vizinhos anteriores e ver se eles são da cor atual */
    for (i = 0; i < regiao; i++) {
        if (mapa[regiao][i] && cores[i] == cor) {
            return 0;
        }
    }

    return 1;
}

int quatroCores(int** mapa, int n, int* cores) {
    pilha* pilhaDeCores;
    int regiao, cor, ok, x;

    /* Inicializa o vetor cores com -1, isto é, não colorido ainda */
    for (x = 0; cores[x]; x++) {
        cores[x] = -1;
    }

    pilhaDeCores = criaPilha(n);

    regiao = 0;
    cor = 0;

    while (regiao < n) {
        ok = 0;

        while (!ok && cor < 4) {
            if (corDisponivel(mapa, cores, regiao, cor)) {
                ok = 1;
            } else {
                cor++;
            }
        }

        if (ok) {
            cores[regiao] = cor;
            empilha(pilhaDeCores, cor);

            regiao++;
            cor = 0;
        } else { /* backtrack */
            if (pilhaVazia(pilhaDeCores)) {
                printf("Não há solucão");
                destroiPilha(pilhaDeCores);
                return 0;
            }

            cores[regiao] = -1;
            cor = desempilha(pilhaDeCores);
            cor++;
            regiao--;
        }
    }

    for (x = 0; x < n; x++) {
        printf("Região %d: Cor %d\n",x, cores[x]);
    }
    return 1;
}

int main() {
    int tam, i, j;
    int** matrizVizinhanca;
    int * cores;

    if (!scanf("%d", &tam)) {
        exit(1);
    }
    printf("\n");
    matrizVizinhanca = alocaMatriz(tam, tam);

    for(i = 0; i < tam; i++) {
        for(j = 0; j < tam; j++) {
            if (!scanf("%d", &matrizVizinhanca[i][j])) {
                exit(1);
            }
        }
    }
    cores = alocaVetor(tam);

    quatroCores(matrizVizinhanca, tam, cores);
    liberaMatriz(matrizVizinhanca, tam);
    return 0;
}
